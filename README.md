# Build status

[![build status](https://gitlab.coko.foundation/pubsweet/pubsweet-server/badges/master/build.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-server/builds)
[![coverage report](https://gitlab.coko.foundation/pubsweet/pubsweet-server/badges/master/coverage.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-server/commits/master)

# Description

This is the PubSweet server, to be used as a dependency in PubSweet apps.

# Development
- Check out our GitLab repository: https://gitlab.coko.foundation/pubsweet/pubsweet-server

# Short-term roadmap
- 0.9 - Current version
- 0.10 - Authorization middleware
